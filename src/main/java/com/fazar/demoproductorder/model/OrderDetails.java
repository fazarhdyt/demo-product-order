package com.fazar.demoproductorder.model;

import java.math.BigDecimal;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;
import javax.validation.constraints.NotNull;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class OrderDetails {

    @EmbeddedId
    private OrderDetailsId id;

    @ManyToOne
    @NotNull
    @MapsId("orders_id")
    @JoinColumn(name = "orders_id")
    private Orders orders;

    @ManyToOne
    @NotNull
    @MapsId("product_id")
    @JoinColumn(name = "product_id")
    private Product product;

    @NotNull
    private int quantity;

    private BigDecimal orderPrice;
}
