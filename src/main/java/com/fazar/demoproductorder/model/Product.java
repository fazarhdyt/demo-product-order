package com.fazar.demoproductorder.model;

import java.math.BigDecimal;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotNull;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Product {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long productId;

    @ManyToOne
    @NotNull
    @JoinColumn(name = "category_id", referencedColumnName = "categoryId")
    private Category category;

    private String productName;
    private int productQuantity;
    private BigDecimal productPrice;

    @OneToMany(mappedBy = "product")
    private List<OrderDetails> orderDetails;
}
