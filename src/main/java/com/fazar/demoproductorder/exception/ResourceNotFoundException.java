package com.fazar.demoproductorder.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@ResponseStatus(value = HttpStatus.NOT_FOUND)
public class ResourceNotFoundException extends RuntimeException {

    private String fieldResource;
    private String fieldName;
    private Object fieldValue;

    public ResourceNotFoundException(String fieldResource, String fieldName, Object fieldValue) {
        super(String.format("%s not found with %s : %s", fieldResource, fieldName, fieldValue));
        this.fieldResource = fieldResource;
        this.fieldName = fieldName;
        this.fieldValue = fieldValue;
    }
}
