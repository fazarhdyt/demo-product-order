package com.fazar.demoproductorder.services;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fazar.demoproductorder.dto.OrderDetailsDto;
import com.fazar.demoproductorder.exception.ResourceNotFoundException;
import com.fazar.demoproductorder.model.OrderDetails;
import com.fazar.demoproductorder.model.OrderDetailsId;
import com.fazar.demoproductorder.model.Orders;
import com.fazar.demoproductorder.model.Product;
import com.fazar.demoproductorder.repository.OrderDetailsRepository;
import com.fazar.demoproductorder.repository.OrdersRepository;
import com.fazar.demoproductorder.repository.ProductRepository;

@Service
public class OrderDetailsService {

    @Autowired
    private OrderDetailsRepository orderDetailsRepository;

    @Autowired
    private ProductRepository productRepository;

    @Autowired
    private OrdersRepository ordersRepository;

    @Autowired
    private ModelMapper modelMapper;

    public void save(OrderDetails orderDetails) {
        OrderDetailsId id = new OrderDetailsId(orderDetails.getOrders().getOrdersId(),
                orderDetails.getProduct().getProductId());
        OrderDetails newOrderDetails = new OrderDetails();
        Product product = productRepository.findById(orderDetails.getProduct().getProductId()).get();
        Orders order = ordersRepository.findById(orderDetails.getOrders().getOrdersId()).get();
        newOrderDetails.setId(id);
        newOrderDetails.setProduct(product);
        newOrderDetails.setOrders(order);
        newOrderDetails.setQuantity(orderDetails.getQuantity());
        newOrderDetails
                .setOrderPrice(product.getProductPrice().multiply(BigDecimal.valueOf(orderDetails.getQuantity())));
        orderDetailsRepository.save(newOrderDetails);
    }

    public OrderDetailsDto findById(Long productId, Long ordersId) {
        OrderDetailsId id = new OrderDetailsId(ordersId, productId);
        Optional<OrderDetails> orderDetails = orderDetailsRepository.findById(id);
        if (!orderDetails.isPresent()) {
            throw new ResourceNotFoundException("orderDetails", "id", id);
        }
        return convertToDto(orderDetailsRepository.findById(id).get());
    }

    public List<OrderDetailsDto> findAll() {
        List<OrderDetails> listOrderDetails = orderDetailsRepository.findAll();
        List<OrderDetailsDto> listDto = new ArrayList<>();
        for (OrderDetails entity : listOrderDetails) {
            OrderDetailsDto ordersDto = convertToDto(entity);
            listDto.add(ordersDto);
        }
        return listDto;
    }

    public void updateProduct(Long productId, Long ordersId, OrderDetails orderDetails) {
        OrderDetailsId id = new OrderDetailsId(ordersId, productId);
        Optional<OrderDetails> orderDetails2 = orderDetailsRepository.findById(id);
        if (!orderDetails2.isPresent()) {
            throw new ResourceNotFoundException("orderDetails", "id", id);
        }
        OrderDetails orderDetailsUpdate = orderDetailsRepository.findById(id).get();
        Product product = productRepository.findById(orderDetails.getProduct().getProductId()).get();
        Orders order = ordersRepository.findById(orderDetails.getOrders().getOrdersId()).get();
        orderDetailsUpdate.setId(id);
        orderDetailsUpdate.setProduct(product);
        orderDetailsUpdate.setOrders(order);
        orderDetailsUpdate.setQuantity(orderDetails.getQuantity());
        orderDetailsUpdate
                .setOrderPrice(product.getProductPrice().multiply(BigDecimal.valueOf(orderDetails.getQuantity())));
        orderDetailsRepository.save(orderDetailsUpdate);
    }

    public void deleteById(Long productId, Long ordersId) {
        OrderDetailsId id = new OrderDetailsId(ordersId, productId);
        Optional<OrderDetails> orderDetails = orderDetailsRepository.findById(id);
        if (!orderDetails.isPresent()) {
            throw new ResourceNotFoundException("orderDetails", "id", id);
        }
        orderDetailsRepository.deleteById(id);
    }

    public OrderDetailsDto convertToDto(OrderDetails orderDetails) {
        OrderDetailsDto ordersDto = modelMapper.map(orderDetails, OrderDetailsDto.class);
        return ordersDto;
    }
}
