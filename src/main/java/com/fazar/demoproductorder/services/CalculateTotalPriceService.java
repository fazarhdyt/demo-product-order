package com.fazar.demoproductorder.services;

import java.math.BigDecimal;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fazar.demoproductorder.dto.TotalPriceDto;
import com.fazar.demoproductorder.model.OrderDetails;
import com.fazar.demoproductorder.repository.OrderDetailsRepository;

@Service
public class CalculateTotalPriceService {

    @Autowired
    private OrderDetailsRepository orderDetailsRepository;

    public TotalPriceDto getTotalPrice() {
        List<OrderDetails> listOrderDetails = orderDetailsRepository.findAll();
        BigDecimal total = BigDecimal.ZERO;
        TotalPriceDto totalPrice = new TotalPriceDto();
        for (OrderDetails entity : listOrderDetails) {
            total = total.add(entity.getOrderPrice());

        }
        totalPrice.setTotalPrice(total);
        return totalPrice;
    }
}
