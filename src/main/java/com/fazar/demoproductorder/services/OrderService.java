package com.fazar.demoproductorder.services;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fazar.demoproductorder.dto.OrdersDto;
import com.fazar.demoproductorder.exception.ResourceNotFoundException;
import com.fazar.demoproductorder.model.Orders;
import com.fazar.demoproductorder.repository.OrdersRepository;

@Service
public class OrderService {

    @Autowired
    private OrdersRepository ordersRepository;

    @Autowired
    private ModelMapper modelMapper;

    public OrdersDto findById(Long id) {
        Optional<Orders> orders = ordersRepository.findById(id);
        if (!orders.isPresent()) {
            throw new ResourceNotFoundException("orders", "id", id);
        }
        return convertToDto(ordersRepository.findById(id).get());
    }

    public List<OrdersDto> findAll() {
        List<Orders> listOrders = ordersRepository.findAll();
        List<OrdersDto> listDto = new ArrayList<>();
        for (Orders entity : listOrders) {
            OrdersDto ordersDto = convertToDto(entity);
            listDto.add(ordersDto);
        }
        return listDto;
    }

    public void updateProduct(Long id, Orders orders) {
        Optional<Orders> orders2 = ordersRepository.findById(id);
        if (!orders2.isPresent()) {
            throw new ResourceNotFoundException("orders", "id", id);
        }
        Orders ordersUpdate = ordersRepository.findById(id).get();
        ordersUpdate.setOrdersId(id);
        ordersUpdate.setOrderDate(orders.getOrderDate());
        ordersRepository.save(ordersUpdate);
    }

    public void deleteById(Long id) {
        Optional<Orders> orders = ordersRepository.findById(id);
        if (!orders.isPresent()) {
            throw new ResourceNotFoundException("orders", "id", id);
        }
        ordersRepository.deleteById(id);
    }

    public OrdersDto convertToDto(Orders orders) {
        OrdersDto ordersDto = modelMapper.map(orders, OrdersDto.class);
        return ordersDto;
    }
}
