package com.fazar.demoproductorder.services;

import java.math.BigDecimal;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fazar.demoproductorder.model.OrderDetails;
import com.fazar.demoproductorder.model.OrderDetailsId;
import com.fazar.demoproductorder.model.Orders;
import com.fazar.demoproductorder.model.Product;
import com.fazar.demoproductorder.repository.OrderDetailsRepository;
import com.fazar.demoproductorder.repository.OrdersRepository;
import com.fazar.demoproductorder.repository.ProductRepository;

@Service
public class OrderProductService {

    @Autowired
    private OrdersRepository ordersRepository;

    @Autowired
    private OrderDetailsRepository orderDetailsRepository;

    @Autowired
    private ProductRepository productRepository;

    public void save(Orders orders) {

        orders = ordersRepository.save(orders);

        List<OrderDetails> listOrderDetails = orders.getOrderDetails();

        for (OrderDetails orderDetails : listOrderDetails) {
            OrderDetailsId id = new OrderDetailsId(orders.getOrdersId(),
                    orderDetails.getProduct().getProductId());
            // orders = ordersRepository.save(orders);
            Product product = productRepository.findById(orderDetails.getProduct().getProductId()).get();
            orderDetails.setId(id);
            orderDetails.setProduct(product);
            orderDetails.setOrders(orders);
            orderDetails.setQuantity(orderDetails.getQuantity());
            orderDetails
                    .setOrderPrice(product.getProductPrice().multiply(BigDecimal.valueOf(orderDetails.getQuantity())));
            orderDetailsRepository.save(orderDetails);
        }
    }
}
