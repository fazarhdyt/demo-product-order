package com.fazar.demoproductorder.services;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.fazar.demoproductorder.dto.ProductDto;
import com.fazar.demoproductorder.exception.ResourceNotFoundException;
import com.fazar.demoproductorder.model.Product;
import com.fazar.demoproductorder.repository.ProductRepository;

@Service
public class ProductService {

    @Autowired
    private ProductRepository productRepository;

    @Autowired
    private ModelMapper modelMapper;

    public ProductDto findById(Long id) {
        Optional<Product> product = productRepository.findById(id);
        if (!product.isPresent()) {
            throw new ResourceNotFoundException("product", "id", id);
        }
        return convertToDto(productRepository.findById(id).get());
    }

    public List<ProductDto> findAll() {
        List<Product> listProducts = productRepository.findAll();
        List<ProductDto> listDto = new ArrayList<>();
        for (Product entity : listProducts) {
            ProductDto productDto = convertToDto(entity);
            listDto.add(productDto);
        }
        return listDto;
    }

    public List<ProductDto> findAllProductSort() {
        List<Product> listProducts = productRepository.findAllProducts(Sort.by("productName"));
        List<ProductDto> listDto = new ArrayList<>();
        for (Product entity : listProducts) {
            ProductDto productDto = convertToDto(entity);
            listDto.add(productDto);
        }
        return listDto;
    }

    public Page<ProductDto> findAllProductsWithPagination(Pageable Pageable) {
        Page<Product> listProducts = productRepository.findAllProductsWithPagination(Pageable);
        List<ProductDto> listDto = new ArrayList<>();
        for (Product entity : listProducts) {
            ProductDto productDto = convertToDto(entity);
            listDto.add(productDto);
        }
        return new PageImpl<>(listDto, Pageable, listProducts.getTotalElements());
    }

    public ProductDto findProductByNameNamedParams(String name) {
        Product products = productRepository.findProductByNameNamedParams(name);
        ProductDto productDto = convertToDto(products);
        return productDto;

    }

    public List<ProductDto> findProductByNameContains(String name) {
        List<Product> listProducts = productRepository.findProductByNameLike("%" + name + "%");
        List<ProductDto> listDto = new ArrayList<>();
        for (Product entity : listProducts) {
            ProductDto productDto = convertToDto(entity);
            listDto.add(productDto);
        }
        return listDto;
    }

    public List<ProductDto> findAllProductList() {
        List<String> listProduct = (Arrays.asList("Printer Canon", "Keyboard Logitech"));
        List<Product> listProducts = productRepository.findProductList(listProduct);
        List<ProductDto> listDto = new ArrayList<>();
        for (Product entity : listProducts) {
            ProductDto productDto = convertToDto(entity);
            listDto.add(productDto);
        }
        return listDto;
    }

    public void updateProduct(Long id, Product product) {
        Optional<Product> product2 = productRepository.findById(id);
        if (!product2.isPresent()) {
            throw new ResourceNotFoundException("product", "id", id);
        }
        Product productUpdate = productRepository.findById(id).get();
        productUpdate.setProductId(id);
        productUpdate.setProductName(product.getProductName());
        productUpdate.setProductQuantity(product.getProductQuantity());
        productUpdate.setProductPrice(product.getProductPrice());
        productUpdate.setCategory(product.getCategory());
        productRepository.save(productUpdate);
    }

    public void deleteById(Long id) {
        Optional<Product> product = productRepository.findById(id);
        if (!product.isPresent()) {
            throw new ResourceNotFoundException("product", "id", id);
        }
        productRepository.deleteById(id);
    }

    public ProductDto convertToDto(Product product) {
        ProductDto productDto = modelMapper.map(product, ProductDto.class);
        return productDto;
    }
}
