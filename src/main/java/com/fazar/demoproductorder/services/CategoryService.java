package com.fazar.demoproductorder.services;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fazar.demoproductorder.dto.CategoryDto;
import com.fazar.demoproductorder.exception.ResourceNotFoundException;
import com.fazar.demoproductorder.model.Category;
import com.fazar.demoproductorder.repository.CategoryRepository;

@Service
public class CategoryService {

    @Autowired
    private CategoryRepository categoryRepository;

    @Autowired
    private ModelMapper modelMapper;

    public CategoryDto findById(Long id) {
        Optional<Category> category = categoryRepository.findById(id);
        if (!category.isPresent()) {
            throw new ResourceNotFoundException("category", "id", id);
        }
        return convertToDto(categoryRepository.findById(id).get());
    }

    public List<CategoryDto> findAll() {
        List<Category> listCategories = categoryRepository.findAll();
        List<CategoryDto> listDto = new ArrayList<>();
        for (Category entity : listCategories) {
            CategoryDto categoryDto = convertToDto(entity);
            listDto.add(categoryDto);
        }
        return listDto;
    }

    public void updateCategory(Long id, Category category) {
        Optional<Category> category2 = categoryRepository.findById(id);
        if (!category2.isPresent()) {
            throw new ResourceNotFoundException("category", "id", id);
        }
        Category categoryUpdate = categoryRepository.findById(id).get();
        categoryUpdate.setCategoryId(id);
        categoryUpdate.setCategoryName(category.getCategoryName());
        categoryRepository.save(categoryUpdate);
    }

    public void deleteById(Long id) {
        Optional<Category> category = categoryRepository.findById(id);
        if (!category.isPresent()) {
            throw new ResourceNotFoundException("category", "id", id);
        }
        categoryRepository.deleteById(id);
    }

    public CategoryDto convertToDto(Category category) {
        CategoryDto categoryDto = modelMapper.map(category, CategoryDto.class);
        return categoryDto;
    }

}
