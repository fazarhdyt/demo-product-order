package com.fazar.demoproductorder.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fazar.demoproductorder.model.Orders;
import com.fazar.demoproductorder.services.OrderProductService;

@RestController
@RequestMapping("/api/orderproduct")
public class OrderProductController {

    @Autowired
    private OrderProductService orderProductService;

    @PostMapping
    public void create(@Valid @RequestBody Orders orders) {
        orderProductService.save(orders);
    }
}
