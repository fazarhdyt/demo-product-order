package com.fazar.demoproductorder.controller;

import java.util.List;

import javax.transaction.Transactional;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.fazar.demoproductorder.dto.ProductDto;
import com.fazar.demoproductorder.model.Product;
import com.fazar.demoproductorder.repository.ProductRepository;
import com.fazar.demoproductorder.services.ProductService;

@RestController
@RequestMapping("/api/product")
public class ProductController {

    @Autowired
    private ProductRepository productRepository;

    @Autowired
    private ProductService productService;

    @PostMapping
    public void create(@Valid @RequestBody Product product) {
        productRepository.save(product);
    }

    @GetMapping
    public List<ProductDto> findAll() {
        return productService.findAll();
    }

    @GetMapping("/sort")
    public List<ProductDto> findAllProductSort() {
        return productService.findAllProductSort();
    }

    @GetMapping("/page")
    public Page<ProductDto> getAllPosts(Pageable pageable) {
        return productService.findAllProductsWithPagination(pageable);
    }

    @GetMapping("/findbyname")
    public ProductDto findByName(String name) {
        return productService.findProductByNameNamedParams(name);
    }

    @GetMapping("/findbynamelike")
    public List<ProductDto> findByNameLike(String name) {
        return productService.findProductByNameContains(name);
    }

    @GetMapping("/list")
    public List<ProductDto> findProductList() {
        return productService.findAllProductList();
    }

    @GetMapping("/{id}")
    public ProductDto findById(@PathVariable Long id) {
        return productService.findById(id);
    }

    @PutMapping("/{id}")
    public void update(@PathVariable Long id, @Valid @RequestBody Product product) {
        productService.updateProduct(id, product);
    }

    @PutMapping
    @Transactional
    public void updateProductSetQuantityForName(@RequestParam Integer quantity, @RequestParam String productName) {
        productRepository.updateProductSetQuantityForName(quantity, productName);
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable Long id) {
        productService.deleteById(id);
    }
}
