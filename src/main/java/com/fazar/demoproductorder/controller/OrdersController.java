package com.fazar.demoproductorder.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fazar.demoproductorder.dto.OrdersDto;
import com.fazar.demoproductorder.model.Orders;
import com.fazar.demoproductorder.repository.OrdersRepository;
import com.fazar.demoproductorder.services.OrderService;

@RestController
@RequestMapping("/api/orders")
public class OrdersController {

    @Autowired
    private OrdersRepository ordersRepository;

    @Autowired
    private OrderService orderService;

    @PostMapping
    public void create(@Valid @RequestBody Orders orders) {
        ordersRepository.save(orders);
    }

    @GetMapping
    public List<OrdersDto> findAll() {
        return orderService.findAll();
    }

    @GetMapping("/{id}")
    public OrdersDto findById(@PathVariable Long id) {
        return orderService.findById(id);
    }

    @PutMapping("/{id}")
    public void update(@PathVariable Long id, @Valid @RequestBody Orders orders) {
        orderService.updateProduct(id, orders);
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable Long id) {
        orderService.deleteById(id);
    }
}
