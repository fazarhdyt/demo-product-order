package com.fazar.demoproductorder.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.fazar.demoproductorder.dto.OrderDetailsDto;
import com.fazar.demoproductorder.model.OrderDetails;
import com.fazar.demoproductorder.services.OrderDetailsService;

@RestController
@RequestMapping("/api/orderdetails")
public class OrderDetailsController {

    @Autowired
    private OrderDetailsService orderDetailsService;

    @PostMapping
    public void create(@Valid @RequestBody OrderDetails orderDetails) {
        orderDetailsService.save(orderDetails);
    }

    @GetMapping
    public List<OrderDetailsDto> findAll() {
        return orderDetailsService.findAll();
    }

    @GetMapping("/id")
    public OrderDetailsDto findById(@RequestParam Long productId, @RequestParam Long ordersId) {
        return orderDetailsService.findById(productId, ordersId);
    }

    @PutMapping
    public void update(@RequestParam Long productId, @RequestParam Long ordersId,
            @Valid @RequestBody OrderDetails orderDetails) {
        orderDetailsService.updateProduct(productId, ordersId, orderDetails);
    }

    @DeleteMapping
    public void delete(@RequestParam Long productId, @RequestParam Long ordersId) {
        orderDetailsService.deleteById(productId, ordersId);
    }
}
