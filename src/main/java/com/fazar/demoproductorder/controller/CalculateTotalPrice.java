package com.fazar.demoproductorder.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fazar.demoproductorder.dto.TotalPriceDto;
import com.fazar.demoproductorder.services.CalculateTotalPriceService;

@RestController
@RequestMapping("/api/totalprice")
public class CalculateTotalPrice {

    @Autowired
    private CalculateTotalPriceService calculateTotalPriceService;

    @GetMapping
    public TotalPriceDto getTotalPrice() {
        return calculateTotalPriceService.getTotalPrice();
    }
}
