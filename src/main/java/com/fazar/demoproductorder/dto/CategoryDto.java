package com.fazar.demoproductorder.dto;

import lombok.Data;

@Data
public class CategoryDto {

    private Long categoryId;
    private String categoryName;
}
