package com.fazar.demoproductorder.dto;

import java.math.BigDecimal;

import com.fazar.demoproductorder.model.OrderDetailsId;

import lombok.Data;

@Data
public class OrderDetailsDto {

    private OrderDetailsId id;
    private OrdersDto orders;
    private ProductDto product;
    private int quantity;
    private BigDecimal orderPrice;
}
