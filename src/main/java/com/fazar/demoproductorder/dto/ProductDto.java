package com.fazar.demoproductorder.dto;

import java.math.BigDecimal;

import lombok.Data;

@Data
public class ProductDto {

    private Long productId;
    private CategoryDto category;
    private String productName;
    private int productQuantity;
    private BigDecimal productPrice;
}
