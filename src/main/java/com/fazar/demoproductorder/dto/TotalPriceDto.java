package com.fazar.demoproductorder.dto;

import java.math.BigDecimal;

import lombok.Data;

@Data
public class TotalPriceDto {
    // private OrdersDto orders;
    private BigDecimal totalPrice;
}
