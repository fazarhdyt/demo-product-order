package com.fazar.demoproductorder.dto;

import java.util.Date;

import lombok.Data;

@Data
public class OrdersDto {

    private Long ordersId;
    private Date orderDate;
}
