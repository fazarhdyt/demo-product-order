package com.fazar.demoproductorder.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.fazar.demoproductorder.model.OrderDetails;
import com.fazar.demoproductorder.model.OrderDetailsId;

@Repository
public interface OrderDetailsRepository extends JpaRepository<OrderDetails, OrderDetailsId> {

}
