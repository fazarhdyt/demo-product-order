package com.fazar.demoproductorder.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.fazar.demoproductorder.model.Product;

@Repository
public interface ProductRepository extends JpaRepository<Product, Long> {

    // List<Product> findByProductNameContains(String name); test git stash
    @Query("SELECT p FROM Product p WHERE p.productName LIKE :name")
    public List<Product> findProductByNameLike(@Param("name") String name);

    @Query(value = "SELECT p FROM Product p")
    List<Product> findAllProducts(Sort sort);

    @Query(value = "SELECT p FROM Product p ORDER BY productId")
    Page<Product> findAllProductsWithPagination(Pageable pageable);

    @Query("SELECT p FROM Product p WHERE p.productName = :name")
    Product findProductByNameNamedParams(@Param("name") String name);

    @Query(value = "SELECT p FROM Product p WHERE p.productName IN :names")
    List<Product> findProductList(@Param("names") List<String> product);

    @Modifying
    @Query("UPDATE Product p SET p.productQuantity = :quantity WHERE p.productName = :name")
    void updateProductSetQuantityForName(@Param("quantity") Integer quantity, @Param("name") String name);
}
